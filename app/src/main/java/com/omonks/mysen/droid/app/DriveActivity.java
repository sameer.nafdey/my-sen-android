package com.omonks.mysen.droid.app;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.File.Labels;
import com.google.api.services.drive.model.FileList;

import net.tawacentral.roger.secrets.FileUtils;
import net.tawacentral.roger.secrets.LoginActivity;
import net.tawacentral.roger.secrets.OS;
import net.tawacentral.roger.secrets.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class DriveActivity extends Activity implements View.OnClickListener {

    private static final int REQUEST_ACCOUNT_PICKER = 1;
    private static final int REQUEST_AUTHORIZATION = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final String APPLICATION_NAME = "MySen DRIVE app";

    public static final String RELOAD_REQUIRED = "DRIVE FILE SAVED AND IT REQUIRES RELOAD";

    private static final int RC_STORAGE_DRIVE_SAVE = 100;


    @Override
    public java.io.File getDataDir() {
        return super.getDataDir();
    }

    private File currentFileOnDrive = null;
    private static Drive service;
    private GoogleAccountCredential credential;

    private TextView tvWelcome = null;
    private ProgressBar progressBar = null;
    private TextView tvProgress = null;
    private Button btnChangeAccount = null;
    private Button btnSelectAccount = null;
    private Button btnSyncNow = null;
    private TextView tvLastSyncDate = null;
    private RelativeLayout relativeLayoutSyncDetails = null;

    private boolean isProgressAnimVisible = false;

    /**
     * Tag for logging purposes.
     */
    public static final String LOG_TAG = "DriveActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive);

        tvWelcome = (TextView) findViewById(R.id.drive_welcome);
        progressBar = (ProgressBar) findViewById(R.id.progressBarDriveBackup);
        tvProgress = (TextView) findViewById(R.id.progressText);
        btnChangeAccount = (Button) findViewById(R.id.btnChangeDriveAccount);
        btnSelectAccount = (Button) findViewById(R.id.btnSelectAccount);
        btnSyncNow = (Button) findViewById(R.id.btnSyncNow);
        tvLastSyncDate = (TextView) findViewById(R.id.tvLastSyncDate);
        relativeLayoutSyncDetails = (RelativeLayout) findViewById(R.id.layoutDriveSyncDetails);

        tvLastSyncDate.setText("");

        btnChangeAccount.setOnClickListener(this);
        btnSelectAccount.setOnClickListener(this);
        btnSyncNow.setOnClickListener(this);

        credential = GoogleAccountCredential.usingOAuth2(this.getApplicationContext(), Collections.singleton(DriveScopes.DRIVE));
        String accName = DriveUtils.getStoredAccountName(this.getBaseContext());

        if (accName == null || accName.trim().length() <= 0) {
            relativeLayoutSyncDetails.setVisibility(View.GONE);
//			startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
        } else {
            credential.setSelectedAccountName(accName);
            updateWelcomeMessage(accName);
            runOnUiThread(new UpdateLastSyncOnUI());
        }

        //have to add this condition because onCreate gets called even when screen orientation changes.
        if (isProgressAnimVisible) {

        } else {
            isProgressAnimVisible = false;
            progressBar.setVisibility(View.INVISIBLE);
            tvProgress.setVisibility(View.INVISIBLE);
        }

    }

	/*private class AsyncTaskFindFileInDrive extends AsyncTask<Void, Integer, File> {

		@Override
		protected File doInBackground(Void... params) {
			try {
				Files.List request = service.files().list();
				request.setQ("title = '"+FileUtils.SECRETS_FILE_NAME+"'");
				
				FileList files = request.execute();
//				Log.d(LOG_TAG, "Total File count: "+files.size());
				
				if(files!=null){
					List<File> fList = files.getItems();
					Log.d(LOG_TAG, "Total File count: "+fList.size());
					for(int i=0; i<fList.size(); i++){
						File f = fList.get(i);
						Labels lbls = f.getLabels();
						if(lbls.getTrashed().booleanValue()==false){
							Log.d(LOG_TAG, "File Title: "+f.getTitle());
							Log.d(LOG_TAG, "File ID: "+f.getId());
							Log.d(LOG_TAG, "File Extension :"+f.getFileExtension());
							Log.d(LOG_TAG, "File Modified Date: "+f.getModifiedDate());
							Log.d(LOG_TAG, "File Size: "+(f.getFileSize()/1024));
							
							if(f.getTitle().trim().equalsIgnoreCase(FileUtils.SECRETS_FILE_NAME)){
								currentFileOnDrive = f;
							}else{
								currentFileOnDrive = null;
							}
							if(currentFileOnDrive!=null){
								break;
							}
						}
					}
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				return currentFileOnDrive;
			}
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(File result) {
			if(result==null){
				saveFileToDrive();
			}else{
				Log.d(LOG_TAG, "File is already available in Drive, hence we need to update it here instead of insert.");
				updateExistingDriveFile(result);
			}
			super.onPostExecute(result);
		}
		
	}*/

    private class DriveUpdateAsyncTask extends AsyncTask<GoogleAccountCredential, Integer, Integer> {

        private static final int STATUS_FAILED = -1;
        private static final int STATUS_FAILED_AUTH = -2;
        private static final int STATUS_FAILED_STORAGE_PERMISSION = -3;

        private static final int STATUS_SUCCESS_UPDATE = 0;
        private static final int STATUS_SUCCESS_CREATE = 1;
        private static final int STATUS_SUCCESS_RELOAD_REQUIRED = 2;

        private static final int RC_STORAGE_SAVE_DRIVE_FILE = 1;

        @Override
        protected void onPreExecute() {
            isProgressAnimVisible = true;
            progressBar.setVisibility(View.VISIBLE);
            tvProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(GoogleAccountCredential... params) {
            long startTime = System.currentTimeMillis();
            Log.d(LOG_TAG, "AsyncTaskGetToken doInBackground started. " + startTime);

            int status = STATUS_FAILED;
            try {

                params[0].getToken();
                service = getDriveService(params[0]);

                //since we have got token again, lets go ahead and check for existing file.

                Files.List request = service.files().list();
                request.setQ("title = '" + FileUtils.SECRETS_FILE_NAME + "'");

                FileList files = request.execute();
//				Log.d(LOG_TAG, "Total File count: "+files.size());

                if (files != null) {
                    List<File> fList = files.getItems();
                    Log.d(LOG_TAG, "Total File count: " + fList.size());
                    for (int i = 0; i < fList.size(); i++) {
                        File f = fList.get(i);
                        Labels lbls = f.getLabels();
                        if (lbls.getTrashed().booleanValue() == false) {
                            Log.d(LOG_TAG, "File Title: " + f.getTitle());
                            Log.d(LOG_TAG, "File ID: " + f.getId());
                            Log.d(LOG_TAG, "File Extension :" + f.getFileExtension());
                            Log.d(LOG_TAG, "File Modified Date: " + f.getModifiedDate());
                            Log.d(LOG_TAG, "File Size: " + (f.getFileSize() / 1024));

                            if (f.getTitle().trim().equalsIgnoreCase(FileUtils.SECRETS_FILE_NAME)) {
                                currentFileOnDrive = f;
                            } else {
                                currentFileOnDrive = null;
                            }
                            if (currentFileOnDrive != null) {
                                break;
                            }
                        }
                    }
                }

                if (currentFileOnDrive != null) {
                    //File already present Lets UPDATE it on Drive.

                    // First retrieve the file from the API.
//				    File file = service.files().get(existingFileInDrive.getId()).execute();

                    if(LoginActivity.getSecrets().isEmpty()){
                        if (OS.ensureStoragePermission(DriveActivity.this, RC_STORAGE_DRIVE_SAVE)) {

                            if(FileUtils.saveDriveFileContentToSDCard(downloadFile(service, currentFileOnDrive))){
                                status = STATUS_SUCCESS_RELOAD_REQUIRED;
                            }
                        }else{
                            status = STATUS_FAILED_STORAGE_PERMISSION;
                        }

                    }else{
                        // File's binary content
                        java.io.File fileContent = FileUtils.getSecretsFileForDrive(getBaseContext());
                        FileContent mediaContent = new FileContent("text/plain", fileContent);

                        com.google.api.services.drive.model.File fileUpdated = service.files().update(currentFileOnDrive.getId(), currentFileOnDrive, mediaContent).execute();

                        if (fileUpdated != null) {
                            Log.d(LOG_TAG, "Existing file updated : File ID: " + fileUpdated.getId());
                            Log.d(LOG_TAG, "Modified Time: " + fileUpdated.getModifiedDate());
                            status = STATUS_SUCCESS_UPDATE;
                        }
                    }

                } else {
                    //File is not present Lets CREATE it on Drive

                    // File's binary content
                    java.io.File fileContent = FileUtils
                            .getSecretsFileForDrive(getBaseContext());
                    FileContent mediaContent = new FileContent("text/plain",
                            fileContent);

                    // File's metadata.
                    com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
                    body.setTitle(fileContent.getName());
                    body.setMimeType("text/plain");
                    body.setFileExtension("mysen");

                    com.google.api.services.drive.model.File file = service.files().insert(body, mediaContent).execute();
                    if (file != null) {
                        Log.d(LOG_TAG, "New File uploaded: " + file.getTitle());
                        status = STATUS_SUCCESS_CREATE;
                    }
                }

            } catch (final UserRecoverableAuthException e) {
                status = STATUS_FAILED_AUTH;

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });


            } catch (IOException e) {
                e.printStackTrace();
//				runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive! Reason - "+e.getMessage()));
            } catch (Exception x) {
                x.printStackTrace();
//				runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive! Reason - "+x.getMessage()));
            } finally {
//				long endTime = System.currentTimeMillis(); 
//				Log.d(LOG_TAG, "AsyncTaskGetToken doInBackground completed. "+endTime);
//				Log.d(LOG_TAG, "AsyncTaskGetToken doInBackground total time taken = "+(endTime-startTime)+" Milliseconds");
                return status;
            }
        }

        /**
         * Download a file's content.
         *
         * @param service Drive API service instance.
         * @param file Drive File instance.
         * @return InputStream containing the file's content if successful,
         *         {@code null} otherwise.
         */
        private InputStream downloadFile(Drive service, File file) {
            if (file.getDownloadUrl() != null && file.getDownloadUrl().length() > 0) {
                try {
                    HttpResponse resp =
                            service.getRequestFactory().buildGetRequest(new GenericUrl(file.getDownloadUrl()))
                                    .execute();
                    return resp.getContent();
                } catch (IOException e) {
                    // An error occurred.
                    e.printStackTrace();
                    return null;
                }
            } else {
                // The file doesn't have any content stored on Drive.
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Integer result) {
            isProgressAnimVisible = true;
            progressBar.setVisibility(View.GONE);
            tvProgress.setVisibility(View.GONE);
            Intent resultIntent = new Intent();

            switch (result) {
                case STATUS_FAILED:
                    Toast.makeText(getBaseContext(), "MySEN could not upload data on Google Drive!", Toast.LENGTH_SHORT).show();
                    break;
                case STATUS_FAILED_AUTH:
                    break;
                case STATUS_SUCCESS_UPDATE:
                    DriveUtils.updateLastSyncDate(getBaseContext());
                    if (relativeLayoutSyncDetails.getVisibility() == View.GONE)
                        relativeLayoutSyncDetails.setVisibility(View.VISIBLE);

                    String date = DriveUtils.getLastSyncDateFormatted(getBaseContext());
                    tvLastSyncDate.setText(date);

                    Toast.makeText(getBaseContext(), "Your google drive is updated with latest data!", Toast.LENGTH_SHORT).show();

                    resultIntent.putExtra(RELOAD_REQUIRED, false);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                    break;
                case STATUS_SUCCESS_CREATE:
                    DriveUtils.updateLastSyncDate(getBaseContext());
                    if (relativeLayoutSyncDetails.getVisibility() == View.GONE)
                        relativeLayoutSyncDetails.setVisibility(View.VISIBLE);

                    String date1 = DriveUtils.getLastSyncDateFormatted(getBaseContext());
                    tvLastSyncDate.setText(date1);

                    Toast.makeText(getBaseContext(), "Your encrypted data is uploaded on Google Drive!", Toast.LENGTH_SHORT).show();
                    break;
                case STATUS_SUCCESS_RELOAD_REQUIRED:

                    resultIntent.putExtra(RELOAD_REQUIRED, true);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                    break;
                case STATUS_FAILED_STORAGE_PERMISSION:
                    Toast.makeText(getBaseContext(), "Storage access permission needed to download data.mysen from Google Drive to restore. If you sync further, it will delete the data.mysen content on Google Drive since you did not restored.", Toast.LENGTH_LONG).show();
            }

//			if(result.booleanValue()==true){
//				saveFileToDrive();
//				findExistingFileOnDrive();
//				new AsyncTaskFindFileInDrive().execute();
//			}
//			super.onPostExecute(result);
        }

    }

    private void showProgressAnim() {
        progressBar.post(new Runnable() {

            @Override
            public void run() {
                isProgressAnimVisible = true;
                progressBar.setVisibility(View.VISIBLE);
                tvProgress.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideProgressAnim() {
        progressBar.post(new Runnable() {

            @Override
            public void run() {
                isProgressAnimVisible = false;
                progressBar.setVisibility(View.INVISIBLE);
                tvProgress.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void updateWelcomeMessage(String accName) {
        String msg = (String) getText(R.string.drive_welcome_acc_selected);
        msg = msg.replace("{acc-name}", accName);
        tvWelcome.setText(msg);
        btnChangeAccount.setVisibility(View.VISIBLE);
        btnSelectAccount.setVisibility(View.INVISIBLE);
    }

    private void resetWelcomeMessage() {
        String msg = (String) getText(R.string.drive_welcome);
        tvWelcome.setText(msg);
        btnChangeAccount.setVisibility(View.INVISIBLE);
        btnSelectAccount.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ACCOUNT_PICKER) {
            if (resultCode == RESULT_OK && data != null
                    && data.getExtras() != null) {
                String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

                DriveUtils.storeSelectedAccountName(getBaseContext(), accountName);
                updateWelcomeMessage(accountName);

                if (accountName != null) {
                    credential.setSelectedAccountName(accountName);
                    service = getDriveService(credential);
                    Log.d(LOG_TAG, "ApplicationName Set in Drive Service Object : " + service.getApplicationName());
//					saveFileToDrive();
                    //new DriveUpdateAsyncTask().execute(credential);
                    checkRequiredPermissions();
                }
            }
        } else if (requestCode == REQUEST_AUTHORIZATION) {
            if (resultCode == Activity.RESULT_OK) {
//				 saveFileToDrive();
                //new DriveUpdateAsyncTask().execute(credential);
                checkRequiredPermissions();
            } else {
                startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // This is an error, abort.
            finish();
        }
    }
	
	/*private void updateExistingDriveFile(final File existingFileInDrive){
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					showProgressAnim();
					
					// First retrieve the file from the API.
//				    File file = service.files().get(existingFileInDrive.getId()).execute();
				      
				   	// File's binary content
					java.io.File fileContent = FileUtils.getSecretsFileForDrive(getBaseContext());
					FileContent mediaContent = new FileContent("text/plain", fileContent);
//					existingFileInDrive.setModifiedDate(new DateTime(System.currentTimeMillis()));
					
					// File's new metadata
//				    file.setTitle(fileContent.getName());
//				    file.setDescription("Created by MySEN on android.");
//				    file.setMimeType("text/plain");

					// File's metadata.
//					com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
//					body.setTitle(fileContent.getName());
//					body.setMimeType("text/plain"); //want to try using application/vnd.google-apps.drive-sdk.YOUR_APP_ID but not sure what will happen if I use it
//					body.setFileExtension("mysen");

					com.google.api.services.drive.model.File fileUpdated = service.files().update(existingFileInDrive.getId(), existingFileInDrive, mediaContent).execute();
					
					if (fileUpdated != null) {
						Log.d(LOG_TAG, "File uploaded: "+fileUpdated.getTitle());
						Log.d(LOG_TAG, "Modified Time: "+fileUpdated.getModifiedDate());
						
						updateLastSyncDate();
						runOnUiThread(new UpdateLastSyncOnUI());
						runOnUiThread(new ToastOnUI("Your google drive is updated with latest data!"));
						
					}else{
						runOnUiThread(new ToastOnUI("MySEN could not update data on Google Drive!"));
					}
				} catch (UserRecoverableAuthIOException e) {
					try {
						startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
					} catch (Exception ex) {
						ex.printStackTrace();
						Log.d(LOG_TAG, "NullPointerException: " + ex.getMessage());
					}
				} catch (IOException e) {
					e.printStackTrace();
					runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive! Reason - "+e.getMessage()));
				} catch (Exception x) {
					x.printStackTrace();
					runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive! Reason - "+x.getMessage()));
				} finally {
					
					hideProgressAnim();
				}
			}
		});
		t.start();
	}*/


    private void saveFileToDrive() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    showProgressAnim();
                    // service = getDriveService(credential);
                    // File's binary content
                    java.io.File fileContent = FileUtils
                            .getSecretsFileForDrive(getBaseContext());
                    FileContent mediaContent = new FileContent("text/plain",
                            fileContent);

                    // File's metadata.
                    com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
                    body.setTitle(fileContent.getName());
                    body.setMimeType("text/plain"); //want to try using application/vnd.google-apps.drive-sdk.YOUR_APP_ID but not sure what will happen if I use it
                    body.setFileExtension("mysen");

                    com.google.api.services.drive.model.File file = service.files().insert(body, mediaContent).execute();
                    if (file != null) {
//						showToast("Photo uploaded: " + file.getTitle());
                        Log.d(LOG_TAG, "File uploaded: " + file.getTitle());

                        DriveUtils.updateLastSyncDate(getBaseContext());
                        runOnUiThread(new UpdateLastSyncOnUI());
                        runOnUiThread(new ToastOnUI("Your encrypted data is uploaded on Google Drive!"));

                    } else {
                        runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive!"));
                    }
                } /*catch (UserRecoverableAuthException e) {
					try {
						startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
					} catch (Exception ex) {
						ex.printStackTrace();
						Log.d(LOG_TAG, "NullPointerException: " + ex.getMessage());
					}
				} */ catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive! Reason - " + e.getMessage()));
                } catch (Exception x) {
                    x.printStackTrace();
                    runOnUiThread(new ToastOnUI("MySEN could not upload data on Google Drive! Reason - " + x.getMessage()));
                } finally {
                    hideProgressAnim();
                }
            }
        });
        t.start();
    }

    private Drive getDriveService(GoogleAccountCredential credential) {
		/*
		 * AccountManager am = (AccountManager)
		 * getSystemService(ACCOUNT_SERVICE);
		 * 
		 * Account[] accounts = am.getAccountsByType("com.google");
		 * 
		 * AccountManagerFuture<Bundle> accFut =
		 * AccountManager.get(getApplicationContext()).getAuthToken(accounts[0],
		 * "oauth2:"+"https://www.googleapis.com/auth/drive", true, null, null);
		 * Bundle authTokenBundle; try { authTokenBundle = accFut.getResult();
		 * 
		 * final String token =
		 * authTokenBundle.get(AccountManager.KEY_AUTHTOKEN).toString();
		 * am.invalidateAuthToken("com.google", token); HttpTransport
		 * httpTransport2 = new NetHttpTransport();
		 * 
		 * Log.d(LOG_TAG,"AuthToken: "+token); JacksonFactory jsonFactory2 = new
		 * JacksonFactory(); Log.d(LOG_TAG,"RefreshToken 123"); //Intent intent
		 * = new Intent(); String
		 * accountname=authTokenBundle.get(AccountManager.
		 * KEY_ACCOUNT_NAME).toString(); Log.d(LOG_TAG,"Account Name : " +
		 * accountname); GoogleCredential credent=new GoogleCredential.Builder()
		 * .setTransport(new NetHttpTransport()) .setJsonFactory(new
		 * JacksonFactory()) .setClientSecrets(CLIENT_ID, CLIENT_SECRET)
		 * .build().setAccessToken(token);
		 * 
		 * HttpTransport httpTransport = new NetHttpTransport();
		 * 
		 * JacksonFactory jsonFactory = new JacksonFactory();
		 * 
		 * Drive.Builder b = new Drive.Builder(httpTransport, jsonFactory,
		 * credent);
		 * 
		 * final Drive drive = b.build();
		 * 
		 * 
		 * Log.d(LOG_TAG,"Drive is been build 789"); return drive; } catch
		 * (OperationCanceledException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (AuthenticatorException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } catch (IOException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); } return
		 * null;
		 */
        return new Drive.Builder(AndroidHttp.newCompatibleTransport(), GsonFactory.getDefaultInstance(), credential).setApplicationName(APPLICATION_NAME).build();
    }

    private class UpdateLastSyncOnUI implements Runnable {
        @Override
        public void run() {
            if (relativeLayoutSyncDetails.getVisibility() == View.GONE)
                relativeLayoutSyncDetails.setVisibility(View.VISIBLE);

            String date = DriveUtils.getLastSyncDateFormatted(getBaseContext());
            tvLastSyncDate.setText(date);
        }
    }

    private class ToastOnUI implements Runnable {

        private String message = null;

        public ToastOnUI(String message) {
            this.message = message;
        }

        @Override
        public void run() {
            Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChangeDriveAccount:
                DriveUtils.storeSelectedAccountName(getBaseContext(), "");
                resetWelcomeMessage();
                currentFileOnDrive = null;
                relativeLayoutSyncDetails.setVisibility(View.GONE);
                startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
                break;
            case R.id.btnSelectAccount:
                startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
                break;
            case R.id.btnSyncNow:
                checkRequiredPermissions();
            default:
                break;
        }
    }

    protected void checkRequiredPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.GET_ACCOUNTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.GET_ACCOUNTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            if (OS.ensureStoragePermission(DriveActivity.this, RC_STORAGE_DRIVE_SAVE)){
                new DriveUpdateAsyncTask().execute(credential);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (grantResults.length == 1 &&
                grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        switch (requestCode) {
            case RC_STORAGE_DRIVE_SAVE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    new DriveUpdateAsyncTask().execute(credential);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


}
