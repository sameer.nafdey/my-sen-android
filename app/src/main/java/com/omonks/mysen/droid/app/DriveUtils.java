package com.omonks.mysen.droid.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.File.Labels;
import com.google.api.services.drive.model.FileList;

import net.tawacentral.roger.secrets.FileUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DriveUtils {
	private static final String PREF_FILE_NAME = "com.omonks.mysen.droid.app.pref";
	private static final String KEY_SELECTED_ACC_NAME_PREF = "SELECTED_ACCOUNT_NAME";
	private static final String KEY_LAST_SYNC_DATE_LONG = "LAST_SYNC_DATE_LONG";
	
	/** Tag for logging purposes. */
	public static final String LOG_TAG = "DriveUtils";

	
	private static SharedPreferences prefs = null;
	
	public static void storeSelectedAccountName(Context context, String accountName){
		if(prefs==null)
			prefs = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(KEY_SELECTED_ACC_NAME_PREF, accountName);
		editor.commit();
	}
	
	public static String getStoredAccountName(Context context){
		if(prefs==null)
			prefs = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		
		return prefs.getString(KEY_SELECTED_ACC_NAME_PREF, "");
	}
	
	public static void updateLastSyncDate(Context context){
		if(prefs==null)
			prefs = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		
		SharedPreferences.Editor editor = prefs.edit();
		editor.putLong(KEY_LAST_SYNC_DATE_LONG, System.currentTimeMillis());
		editor.commit();
//		return prefs.getLong(KEY_LAST_SYNC_DATE_LONG, 0L);
	}
	
	public static String getLastSyncDateFormatted(Context context){
		if(prefs==null)
			prefs = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
		
		long lastSyncDate = prefs.getLong(KEY_LAST_SYNC_DATE_LONG, 0L);
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM dd, yyyy hh:mm aa zzz ");
		
		if(lastSyncDate>0L){
			String formattedDateStr = sdf.format(new Date(lastSyncDate));
			return formattedDateStr;
		}else{
			return "";
		}
	}

	public static void takeBackupOnDrive(Context appContext){
		try {
			
			String accName = DriveUtils.getStoredAccountName(appContext);
			if(accName==null || accName.trim().length()<=0){
				Log.d(LOG_TAG, "Google Drive is not connected with MySEN.");
				return;
			}
			
			GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(appContext, Collections.singleton(DriveScopes.DRIVE));
			credential.setSelectedAccountName(accName);
			
			credential.getToken();
			
			Drive service = getDriveService(credential);
			
			File currentFileOnDrive = null;
			
			//since we have got token again, lets go ahead and check for existing file.
			
			Files.List request = service.files().list();
			request.setQ("title = '"+ FileUtils.SECRETS_FILE_NAME+"'");
			
			FileList files = request.execute();
//			Log.d(LOG_TAG, "Total File count: "+files.size());
			
			if(files!=null){
				List<File> fList = files.getItems();
				Log.d(LOG_TAG, "Total File count: "+fList.size());
				for(int i=0; i<fList.size(); i++){
					File f = fList.get(i);
					Labels lbls = f.getLabels();
					if(lbls.getTrashed().booleanValue()==false){
						Log.d(LOG_TAG, "File Title: "+f.getTitle());
						Log.d(LOG_TAG, "File ID: "+f.getId());
						Log.d(LOG_TAG, "File Extension :"+f.getFileExtension());
						Log.d(LOG_TAG, "File Modified Date: "+f.getModifiedDate());
						Log.d(LOG_TAG, "File Size: "+(f.getFileSize()/1024));
						
						if(f.getTitle().trim().equalsIgnoreCase(FileUtils.SECRETS_FILE_NAME)){
							currentFileOnDrive = f;
						}else{
							currentFileOnDrive = null;
						}
						if(currentFileOnDrive!=null){
							break;
						}
					}
				}
			}
			
			if(currentFileOnDrive!=null){
				//File already present Lets UPDATE it on Drive.
				
				// First retrieve the file from the API.
//			    File file = service.files().get(existingFileInDrive.getId()).execute();
				
				// File's binary content
				java.io.File fileContent = FileUtils.getSecretsFileForDrive(appContext);
				FileContent mediaContent = new FileContent("text/plain", fileContent);
				
				com.google.api.services.drive.model.File fileUpdated = service.files().update(currentFileOnDrive.getId(), currentFileOnDrive, mediaContent).execute();
				
				if (fileUpdated != null) {
					Log.d(LOG_TAG, "Existing file updated : File ID: "+fileUpdated.getId());
					Log.d(LOG_TAG, "Modified Time: "+fileUpdated.getModifiedDate());
				}
			}else{
				//File is not present Lets CREATE it on Drive
				
				// File's binary content
				java.io.File fileContent = FileUtils.getSecretsFileForDrive(appContext);
				FileContent mediaContent = new FileContent("text/plain", fileContent);

				// File's metadata.
				com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();
				body.setTitle(fileContent.getName());
				body.setMimeType("text/plain"); 
				body.setFileExtension("mysen");

				com.google.api.services.drive.model.File file = service.files().insert(body, mediaContent).execute();
				if (file != null) {
					Log.d(LOG_TAG, "New File uploaded: "+file.getTitle());
				}
			}
			
		} catch (final UserRecoverableAuthException e) {
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception x) {
			x.printStackTrace();
		} finally {
		}
	}

	private static Drive getDriveService(GoogleAccountCredential credential) {
		return new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential).build();
	}


}
